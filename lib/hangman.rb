class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @guesser = players[:guesser]
    @referee = players[:referee]
    @incorrect_letters = []
    @guessed_bank = []
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word_length)
    @board = Array.new(secret_word_length)
  end

  def take_turn
    guess = @guesser.guess(@guessed_bank)
    correct_indices = @referee.check_guess(guess)
    update_board(guess, correct_indices)
    @guesser.handle_response(guess, correct_indices)
    display_board
  end

  def update_board(letter, indices)
    indices.each { |idx| @board[idx] = letter }
    @incorrect_letters << letter if indices.empty?
    @guessed_bank << letter
  end

  def play
    introduction
    setup
    while @incorrect_letters.size < 7
      take_turn
      break if won?
    end

    conclude
  end

  private

  def answer(guess)
    if guess == @referee.secret_word
      puts "CONGRATS IT'S OVER!"
    end
  end

  def display_board

    word_display = @board.reduce([]) do |string, char|
      char.nil? ? string << "_" : string << char
    end

    puts word_display.join(" ")
    puts "Wrong guesses (#{@incorrect_letters.size}): \
#{@incorrect_letters.join(", ")}"
  end

  def introduction
    puts "Welcome to the game of Hangman."
    puts "You have 7 strikes to find out what the word is."
    puts "You may guess the answer at any time, but will suffer 2 \
strikes if you are wrong."
    puts "Press ENTER key to begin."
    system("clear") if gets
  end

  def won?
    @board.all?
  end

  def conclude
    if won?
      puts "Congrats! The secret word was #{@referee.secret_word.upcase}."
    else
      puts "Sorry, the secret word was #{@referee.secret_word.upcase}."
    end
  end

end

class HumanPlayer

  attr_accessor :name

  def initialize
    @name = "Human"
  end

  def register_secret_length(length)
    puts "The word is #{length} characters long."
  end

  def guess(board)
    alphabet = ('a'..'z').to_a

    puts "#{name}, please guess a letter from the alphabet."
    guessed_char = gets.chomp

    while board.include?(guessed_char) || !alphabet.include?(guessed_char)
      puts "Please guess a different letter."
      guessed_char = gets.chomp
    end

    guessed_char
  end

  def handle_response(guess, indices)
    system("clear")
    puts "You guessed the letter: #{guess}"
    if indices.empty?
      puts "Whoops! There are no instances of #{guess} in this word."
    else
      puts "There are #{indices.size} instances of #{guess}. Nice!"
    end
  end

end

class ComputerPlayer

  attr_reader :dictionary, :secret_word

  def initialize(dictionary = "dictionary.txt")
    @dictionary = dictionary == "dictionary.txt" ?
      File.readlines(dictionary) : dictionary
    @secret_word = @dictionary.sample.delete("\n")
  end

  def pick_secret_word
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.downcase.chars.each_with_index do |char, idx|
      indices << idx if char == letter
    end

    indices
  end

  def register_secret_length(length)
    candidate_words.reject! { |word| word.length != length }
  end

  def handle_response(guess, indices)
    candidate_words.select! { |word| valid_word(word, indices, guess) }

    if indices.empty?
      puts "Whoops! There are no instances of #{guess} in this word."
    else
      puts "There are #{indices.size} instances of #{guess}. Nice!"
    end

  end

  def guess(board)
    letter_array = candidate_words.join.chars

    sorted_chars = letter_array.uniq.sort_by do |char|
      letter_array.count(char)
    end.reverse

    sorted_chars.each do |char|
      return char unless board.include?(char)
    end

  end

  def candidate_words
    @dictionary.map! { |word| word.delete("\n") }
  end

  private

  def valid_word(word, indices, guess)
    return false if word.count(guess) > indices.size
    indices.all? { |idx| word[idx] == guess }
  end

end

if __FILE__ == $PROGRAM_NAME
  players = {
    guesser: HumanPlayer.new,
    referee: ComputerPlayer.new
  }
  x = Hangman.new(players)
  x.play
end
